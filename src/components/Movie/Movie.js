import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Overdrive from 'react-overdrive';
import './styles.scss';

const config = require('../../config.json');

const Movie = ({ movie }) => (
    <div className="movie">
        <Link to={`${movie.id}`}>
            <Overdrive id={`${movie.id}`}>
                <img
                    src={`${config.tmdb.poster_path}${movie.poster_path}`}
                    alt={`${movie.title}`}
                    className="poster"
                />
            </Overdrive>
        </Link>
    </div>
);

Movie.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number.isRequired,
        poster_path: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
    }).isRequired,
};

export default Movie;
