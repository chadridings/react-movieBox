import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link,
} from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Logo from '../images/logo.svg';
import MoviesList from './MoviesList/MoviesList';
import MoviesDetails from './MovieDetails/MovieDetails';
import './app.scss';

const hello = () => ('hello');
const store = createStore(hello);

const App = () => (
    <Provider store={store}>
        <Router>
            <div className="App">
                <header className="header">
                    <Link to="/">
                        <Logo />
                    </Link>
                </header>
                <Switch>
                    <Route exact path="/" component={MoviesList} />
                    <Route path="/:id" component={MoviesDetails} />
                </Switch>
            </div>
        </Router>
    </Provider>
);

export default App;
