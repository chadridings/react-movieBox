import React, { Component } from 'react';
import Movie from '../Movie/Movie';
import './styles.scss';

const config = require('../../config.json');

class MoviesList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movies: [],
        };
    }

    async componentDidMount() {
        try {
            const fetchUrl = `https://api.themoviedb.org/3/discover/movie?api_key=${config.tmdb.api_key}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;
            const res = await fetch(fetchUrl);
            const data = await res.json();
            this.setState({
                movies: data.results,
            });
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        const { movies } = this.state;

        return (
            <div className="movieGrid">
                { movies.map(movie => <Movie movie={movie} key={movie.id} />) }
            </div>
        );
    }
}

export default MoviesList;
