import React, { Component } from 'react';
import Overdrive from 'react-overdrive';
import './styles.scss';

const config = require('../../config.json');

class MovieDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movie: {},
            backgroundImage: '',
            posterImage: '',
        };
    }

    async componentDidMount() {
        try {
            const { props } = this;
            const fetchUrl = `https://api.themoviedb.org/3/movie/${props.match.params.id}$?api_key=${config.tmdb.api_key}&language=en-US`;
            const res = await fetch(fetchUrl);
            const movie = await res.json();
            this.setState({
                movie,
                backgroundImage: `url(${config.tmdb.backdrop_path}${movie.backdrop_path})`,
                posterImage: `${config.tmdb.poster_path}${movie.poster_path}`,
            });
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        const { movie } = this.state;
        const { backgroundImage } = this.state;
        const { posterImage } = this.state;

        return (
            <div
                className="movieDetails"
                style={{ backgroundImage }}
            >
                <div className="info">
                    <Overdrive id={`${movie.id}`}>
                        <img
                            alt={`${movie.title}`}
                            className="poster"
                            src={posterImage}
                        />
                    </Overdrive>

                    <div className="content">
                        <h1>{movie.title}</h1>
                        <h4>{movie.release_date}</h4>
                        <p>{movie.overview}</p>
                        <p>{`Score: ${movie.vote_average} (${movie.vote_count} votes)`}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default MovieDetails;
